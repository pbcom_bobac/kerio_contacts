# Kerio Contacts exporter. Exports all Contacts *.eml files to one *.vcf
# that can be imported into other server (tested with Google Contacts)
# To do the export just set the twho variables below and run:
#    ruby eml2vcf.rb

# set this to path, where user's contacts are stored (Usually it is
# the Contacts/#msgs folder that should contain a bunch of *.eml files)
input_folder = "/store/kerio/domain.com/user/Contacts/#msgs"

# this is the name of the *.vcf file to which you are exporting to
# after executing the eml2vcf take this file and import it to your
# app
output_file = "./output/domain.com/user.vcf"

open(output_file, 'w') do |f|

  Dir.foreach(input_folder) do |filename|
    next if filename == '.' or filename == '..'
    # Do work on the remaining files & directories
    dir_filename = input_folder + "/" + filename
    puts "File: #{dir_filename}"
  
    line_num=0
    in_vcard = false
    text=File.open(dir_filename).read
    text.gsub!(/\r\n?/, "\n")
    text.each_line do |line|
      if line.start_with? "BEGIN:VCARD" then in_vcard = true end
      if in_vcard
        print "#{line_num += 1} #{line}"
        f.puts line
      end
    end
  end

end
