# Kerio Contacts exporter.
Exports all Contacts *.eml files to one *.vcf
that can be imported into other server (tested with Google Contacts)
To do the export just set the two variables below and run:
`ruby eml2vcf.rb`

Set this to path, where user's contacts are stored (Usually it is
the Contacts/#msgs folder that should contain a bunch of *.eml files)
`input_folder = "/store/kerio/domain.com/user/Contacts/#msgs"`

This is the name of the *.vcf file to which you are exporting to.
After executing the eml2vcf take this file and import it to your
app.
`output_file = "./output/domain.com/user.vcf"`

# Prequisities
Designed and tested on Ubuntu Linux with ruby installed.

# Installation
Just clone this repo `git clone https://gitlab.com/pbcom_bobac/kerio_contacts`, alter the `input_folder` and `output_file` and run.

# Note
It was written as a ten minutes script that, I believe, might be usefull
to others. It exports just one particular user's contacts, as I needed it for exactly two accounts. If you need to export a bunch of accounts, you
will need to extend the script by yourself.

# Disclaimer
The software is provided "as is", use it at your own risk

# License
MIT